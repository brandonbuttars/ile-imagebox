# ILE ImageBox #
This is a simple way to create a modal.  Easily hacked and modified.
### Configure ###
* Include the *latest version of jQuery*.
* Include the *file-imgbox.js* file.
* Create an *img* element with an *identifying class* or *id*.
* Within a document ready function using jQuery add 
    `imgPopUp('className');`