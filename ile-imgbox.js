// imgPopUp function.
var imgPopUp = function(element) {
	jQuery(document).on('click', element, function() {

		var src = jQuery(this).attr('src');

		jQuery('<div class="ile_overlay ile_hidden"><div class="ile_popup ile_hidden"><span class="ile_img_box"><img src="' + src + '" /></span></div></div>').insertAfter('body');

		setTimeout(function() {
			jQuery('.ile_overlay').fadeIn();
		}, 0);

		setTimeout(function() {
			jQuery('.ile_popup').fadeIn();
		}, 100);

	});

	jQuery(document).on('click', '.ile_overlay, .ile_popup', function() {

		jQuery('.ile_popup').fadeOut();

		setTimeout(function() {
			jQuery('.ile_overlay').fadeOut();
		}, 100);

		setTimeout(function() {
			jQuery('.ile_overlay').remove();
		}, 500);

	});
}

// Trigger actions on document ready.
jQuery(function() {

	// Activate popup on element in argument on page load.
	imgPopUp('.thumbnail');

});